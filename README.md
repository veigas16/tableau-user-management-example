# Tableau User Management Example

This is an example library that can be used to manage users (add/update/remove) on Tableau Server. This was originally built to assist a client in automating the addition/removal of users from their Tableau Server environment.

## Requirements and Setup

**Requirements**
* Python 3+

Should work in any version of Python 3 or higher, but was written in Python 3.7.3.

**Setup**
If you want to run the example script:
1. Clone this repo
2. From the root directory of the cloned repo, run `pip install -r requirements.txt`
3. Open `example.py` in your code editor of choice
4. Update lines 4 through 7 with info specific to your Tableau Server instance
5. Save and close
6. Run `python example.py` (depending on your setup, you may need to use `python3 example.py`)

To use `tab_user_mgmt` as library in your project:
1. Clone this repo
2. Copy the `tab_user_mgmt` folder to your project
3. Follow the documentation below to use the library

## `tab_user_mgmt` Library

### TabUserManagement

The TabUserManagement class contains the core functionality of the library.

**TabUserManagement class**

`TabUserManagement(server_base_url, server_api_version)`

Attributes

| Attribute | Type | Required | Default | Description |
| --- | --- | :-: | --- | --- |
| server_base_url | str | Yes | | The url for your Tableau Server instance
| server_api_version | str | Yes | | The REST API version corresponding to the version of Tableau Server you are running. A comprehensive list of server versions can be found [here](https://onlinehelp.tableau.com/current/api/rest_api/en-us/REST/rest_api_concepts_versions.htm#version-availability).
| site_content_url | str | No | `None` | The content URL specifying the Tableau Server site you'd like to login to. If omitted, you will be logged into the Default site. For more info on determining the site content URL, check [here](https://onlinehelp.tableau.com/current/api/rest_api/en-us/REST/rest_api_ref_authentication.htm#sign_in).

Properties

| Property | Type | Default | Description |
| --- | --- | --- | --- |
| auth_valid_for | int | 14,400 | The number of seconds that a REST API auth token is valid for on your Tableau Server instance. By default, this value is 14,400 (240 minutes) but can be modified using `tsm configuration set` and modifying the `wgserver.session.idle_limit` value.
| site_id | str | `None` | **Do not** set this manually, it is set by the library on a `sign_in()` or `sign_out()` call.
| auth_token | str | `None` | **Do not** set this manually, it is set by the library on a `sign_in()` or `sign_out()` call.
| token_fetch_time | datetime | `None` | **Do not** set this manually, it is set by the library on a `sign_in()` or `sign_out()` call.

Example
```python
>>> from tab_user_mgmt import TabUserManagement

>>> tum = TabUserManagement('https://tableau.mycompany.com', '3.4', 'MyTableauSite')
>>> # assuming we set wgserver.session.idle_limit to 100 minutes
>>> tum.auth_valid_for = 100 * 60
```

**TabUserManagement methods**

**`sign_in(username, password)`**

Signs into the Tableau Server REST API using the username and password provided. Since this library focuses on user management, this username/password would, ideally, be the credentials of someone with Server Admin access. At a minimum, it _must_ be a user with Site Administrator access for the site you wish to update.

Arguments

| Argument | Type | Default | Description |
| --- | --- | --- | --- |
| username | str | | The username of the user you are logging in as |
| password | str | | The password of the user you are logging in as |

Example
```python
>>> from tab_user_mgmt import TabUserManagement

>>> tum = TabUserManagement('https://tableau.mycompany.com', '3.4', 'MyTableauSite')
...
>>> tum.sign_in('my_username', 'my_password')
```

**`sign_out()`**

If signed in (i.e. you previously called `sign_in()` and the token is still active), signs out of the Tableau Server REST API and invalidates the associated token.

Example
```python
>>> from tab_user_mgmt import TabUserManagement

>>> tum = TabUserManagement('https://tableau.mycompany.com', '3.4', 'MyTableauSite')
...
>>> tum.sign_out()
```

**`add_user(username, site_role, **kwargs)`**

Adds a new user with the specified site role to the site on Tableau Server

Arguments

| Argument | Type | Default | Description |
| --- | --- | --- | --- |
| username | str | | The username to be associated with the new user. This must be unique across Tableau Server. |
| site_role | TableauSiteRole | | The role that the new user will assume on the site. See documentation for TableauSiteRole below. |

Keyword Arguments

You may, optionally, specify any of the following keyword arguments. If you are using local authentication, you'll need to specify a password at a minimum.

| Argument | Type | Default | Description |
| --- | --- | --- | --- |
| full_name | str | | The full name of the new user |
| email | str | | The email address of the new user |
| password | str | | The password to be associated with the new user. If using LDAP, setting this should not be required. |

Returns

A dictionary containing key values pairs describing the user that was created. Minimally, this dictionary will contain the ID, full name, and site role of the newly created user. If any keyword arguments are specified, those will be present in the dictionary as well.

Example
```python
>>> from tab_user_mgmt import TabUserManagement, TableauSiteRoles

>>> tum = TabUserManagement('https://tableau.mycompany.com', '3.4', 'MyTableauSite')
...
>>> tum.add_user('new_user', TableauSiteRoles.Viewer, full_name='New User', email='newuser@mycompany.com')
{
    'id': '441b8fc6-b4eb-4957-8de4-9787208b1125',
    'name': 'new_user',
    'fullName': 'New User',
    'email': 'newuser@mycompany.com'
}
```

**`get_all_users()`**

Gets all users on the site.

Returns

A list of dictionaries where each dictionary represents a user in the format provided by Tableau's REST API. Generally, the dictionary will contain the following keys: id, name, siteRole, lastLogin, externalAuthUserId.

Example
```python
>>> from tab_user_mgmt import TabUserManagement

>>> tum = TabUserManagement('https://tableau.mycompany.com', '3.4', 'MyTableauSite')
...
>>> tum.get_all_users()
[
    {
        'id': 'a8fd57c1-bf9e-41a1-a788-fd7182522c86',
        'name': 'server_admin',
        'siteRole': 'ServerAdministrator',
        'lastLogin': '2019-07-26T12:47:32Z',
        'externalAuthUserId': ''
    }, {
        'id': 'aa109eee-c2dd-400a-a4c9-572a776da837',
        'name': 'viewer_user',
        'siteRole': 'Viewer',
        'lastLogin': '2019-07-17T15:06:48Z',
        'externalAuthUserId': ''
    }
]
```

**`get_user_id_from_username(username)`**

Utility function that wraps the `get_all_users()` method and returns a user's ID given their username.

Arguments

| Argument | Type | Default | Description |
| --- | --- | --- | --- |
| username | str | | The username to search for

Returns

The ID of the user if a match is found, `None` if no match is found.

Example
```python
>>> from tab_user_mgmt import TabUserManagement

>>> tum = TabUserManagement('https://tableau.mycompany.com', '3.4', 'MyTableauSite')
...
>>> tum.get_user_id_from_username('viewer_user')
'aa109eee-c2dd-400a-a4c9-572a776da837'
```

**`remove_user(username)`**

Arguments

| Argument | Type | Default | Description |
| --- | --- | --- | --- |
| username | str | | The username of the user to remove from the site

Returns

A dictionary describing the user that was removed from the site

Example
```python
>>> from tab_user_mgmt import TabUserManagement

>>> tum = TabUserManagement('https://tableau.mycompany.com', '3.4', 'MyTableauSite')
...
>>> tum.remove_user('viewer_user')
{
	'removedUser': {
		'id': 'aa109eee-c2dd-400a-a4c9-572a776da837',
		'name': 'viewer_user'
	}
}
```

### TableauSiteRoles

The TableauSiteRoles class is an enumeration of valid site roles as specified by Tableau Server's REST API. For more information on valid site roles, check the [Tableau REST API Documentation](https://onlinehelp.tableau.com/current/api/rest_api/en-us/REST/rest_api_ref_usersgroups.htm#add_user_to_site).

| Property | Value |
| --- | --- |
| Creator | 'Creator' |
| Explorer | 'Explorer' |
| ExplorerCanPublish | 'ExplorerCanPublish' |
| SiteAdminstratorExplorer | 'SiteAdministratorExplorer' |
| SiteAdministratorCreator | 'SiteAdministratorCreator' |
| Unlicensed | 'Unlicensed' |
| Viewer | 'Viewer' |

### Errors

The Tableau User Management library implements two custom error types.

`TableauAPIError`

Used to report errors returned from the Tableau REST API. Generally this error message will contain an error code, summary, and details as they are reported by Tableau Server's REST API.

`TableauUserManagementError`

Used to report errors in how this library is used. Generally, these will be errors related to an expired token.