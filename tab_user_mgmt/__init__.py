import requests
from datetime import datetime
from .helpers import TableauAPIError, TableauUserManagementError, TableauSiteRoles, api_error_message

class TabUserManagement(object):
    """Tableau User Management Example Library
    
    Attributes:
        server_base_url (str): The url for your Tableau Server instance.
        server_api_version (str): The REST API version corresponding to your Tableau Server
            version. You can find this value at 
            https://onlinehelp.tableau.com/current/api/rest_api/en-us/REST/rest_api_concepts_versions.htm#version-availability.
        site_content_url (str, optional): The content URL for the site you are logging
            into. More info on determinging your content URL can be found at
            https://onlinehelp.tableau.com/current/api/rest_api/en-us/REST/rest_api_ref_authentication.htm#sign_in
            under the Attribute Values section.
        auth_valid_for (datetime, optional): The number of seconds that an API auth token is valid
            for your Tableau Server. By default, this value is 14,400 (240 minutes) but this value
            can be updated using TSM. Make sure this value is consistent with your server's settings.

    """

    auth_valid_for = 240 * 60
    site_id = None
    auth_token = None
    token_fetch_time = None

    def __init__(self, server_base_url, server_api_version, site_content_url = None):
        self.server_base_url = server_base_url
        self.server_api_version = server_api_version
        self.site_content_url = site_content_url
        self.api_url = '{}/api/{}'.format(
            self.server_base_url,
            self.server_api_version
        )

    def _auth_valid(self):
        """Check that our token is set and still valid"""
        if self.site_id is None or self.auth_token is None or self.token_fetch_time is None:
            return False
        return (datetime.now() - self.token_fetch_time).total_seconds() < self.auth_valid_for
    
    def _build_auth_headers(self, auth_token = None):
        """Build standard headers for a Tableau Server API request"""
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        if auth_token is not None:
            headers['x-tableau-auth'] = auth_token
        return headers

    def sign_in(self, username, password):
        """Sign into your Tableau Server REST API.
        
        Arguments:
            username {str} -- The username to sign in with. This should be a user with server admin privileges.
            password {str} -- The password to sign in with. This should be a user with server admin privileges.
        
        Raises:
            TableauAPIError: An error reported by the Tableau Server REST API.
        """
        url = '{}/auth/signin'.format(
            self.api_url
        )
        payload = {
            'credentials': {
                'name': username,
                'password': password,
                'site': {
                    'contentUrl': '' if self.site_content_url is None else self.site_content_url
                }
            }
        }
        r = requests.post(url, json=payload, headers=self._build_auth_headers())
        response = r.json()
        if 'error' in response:
            raise TableauAPIError(api_error_message(response))
        self.site_id, self.auth_token, self.token_fetch_time = (
            response['credentials']['site']['id'],
            response['credentials']['token'],
            datetime.now()
        )

    def sign_out(self):
        """Sign out of your Tableau Server REST API and invalidate the auth token.
        
        Raises:
            TableauAPIError: An error reported by the Tableau Server REST API.
        """
        if self._auth_valid():
            url = '{}/auth/signout'.format(
                self.api_url
            )
            r = requests.post(url, headers=self._build_auth_headers(self.auth_token))
            if (len(r.text) > 0):
                response = r.json()
                raise TableauAPIError(api_error_message(response))
        self.site_id, self.auth_token, self.token_fetch_time = (
            None,
            None,
            None
        )

    def _api_add_user(self, username, site_role):
        """Call the Tableau Server REST API and add a new user
        
        Arguments:
            username {str} -- The username of the new user
            site_role {TableauSiteRole} -- The site role that the new user will be assigned
        
        Raises:
            TableauAPIError: An error reported by the Tableau Server REST API.
        
        Returns:
            str -- The ID of the new user.
        """
        url = '{}/sites/{}/users'.format(
            self.api_url,
            self.site_id
        )
        payload = {
            'user': {
                'name': username,
                'siteRole': site_role
            }
        }
        r = requests.post(url, json=payload, headers=self._build_auth_headers(self.auth_token))
        response = r.json()
        if 'error' in response:
            raise TableauAPIError(api_error_message(response))
        return response['user']['id']

    def _api_update_user(self, user_id, full_name = None, email = None, password = None, site_role = None):
        """Call the Tableau Server REST API and update a user
        
        Arguments:
            user_id {str} -- The ID of the user to be updated.
        
        Keyword Arguments:
            full_name {str} -- The value to set as the users's full name (default: {None})
            email {str} -- The value to set as the users email (default: {None})
            password {str} -- The value to set as the user's password (default: {None})
            site_role {TableauSiteRole} -- The value to set as the user's site role (default: {None})
        
        Raises:
            TableauAPIError: An error reported by the Tableau Server REST API
        
        Returns:
            dict -- A dictionary containing any keys and values set in the update process
        """
        url = '{}/sites/{}/users/{}'.format(
            self.api_url,
            self.site_id,
            user_id
        )
        payload = {
            'user': {}
        }
        if full_name is not None:
            payload['user']['fullName'] = full_name
        if email is not None:
            payload['user']['email'] = email
        if password is not None:
            payload['user']['password'] = password
        if site_role is not None:
            payload['user']['siteRole'] = site_role
        r = requests.put(url, json=payload, headers=self._build_auth_headers(self.auth_token))
        response = r.json()
        if 'error' in response:
            raise TableauAPIError(api_error_message(response))
        return response

    def add_user(self, username, site_role, full_name = None, email = None, password = None):
        """Add a new user to a Tableau Server site
        
        Arguments:
            username {str} -- The username to assign to the new user
            site_role {TableauSiteRole} -- The site role to assign the new user
        
        Keyword Arguments:
            full_name {str} -- The full name of the new user (default: {None})
            email {str} -- The email address of the new user (default: {None})
            password {str} -- The password of the new user (default: {None})
        
        Raises:
            TableauUserManagementError: A class usage error
        
        Returns:
            dict -- A dictionary describing the newly created user
        """
        if not self._auth_valid():
            raise TableauUserManagementError('You are not signed in or your token has expired. Please sign in.')
        new_user_id = self._api_add_user(username, site_role)
        user_profile = self._api_update_user(new_user_id, full_name = full_name, email = email, password = password)
        return {
            'id': new_user_id,
            **user_profile['user']
        }

    def get_all_users(self):
        """Get all users for the site you are currently signed into on Tableau Server
        
        Raises:
            TableauUserManagementError: A class usage error
        
        Returns:
            list -- A list of users, along with their attributes, on the specified site
        """
        if not self._auth_valid():
            raise TableauUserManagementError('You are not signed in or your token has expired. Please sign in.')
        page_size = 1000
        page_number = 1
        users = []
        while True:
            url = '{}/sites/{}/users?pageSize={}&pageNumber={}'.format(
                self.api_url,
                self.site_id,
                page_size,
                page_number
            )
            r = requests.get(url, headers=self._build_auth_headers(self.auth_token))
            response = r.json()
            users += response['users']['user']
            total_fetched = int(response['pagination']['pageNumber']) * int(response['pagination']['pageSize'])
            if total_fetched >= int(response['pagination']['totalAvailable']):
                break
        return users

    def get_user_id_from_username(self, username):
        """Utility function that makes it easy to find a user ID based on a username
        
        Arguments:
            username {str} -- The username to search for
        
        Raises:
            TableauUserManagementError: A class usage error
        
        Returns:
            str or None -- The ID of the located user or None if no matches are found
        """
        if not self._auth_valid():
            raise TableauUserManagementError('You are not signed in or your token has expired. Please sign in.')
        users = self.get_all_users()
        matches = [u['id'] for u in users if u['name'] == username]
        if len(matches) != 1:
            return None
        return matches[0]

    def _api_remove_user(self, user_id):
        """Call the Tableau Server API and remove a user
        
        Arguments:
            user_id {str} -- The ID of the user to be removed
        
        Raises:
            TableauAPIError: An error reported by the Tableau Server REST API
        
        Returns:
            None -- If successful, return None
        """
        url = '{}/sites/{}/users/{}'.format(
            self.api_url,
            self.site_id,
            user_id
        )
        r = requests.delete(url, headers=self._build_auth_headers(self.auth_token))
        if (len(r.text) > 0):
            response = r.json()
            raise TableauAPIError(api_error_message(response))
        return None

    def remove_user(self, username):
        """Remove a user from the site you are currently logged into on Tableau Server
        
        Arguments:
            username {str} -- The username of the user to remove
        
        Raises:
            TableauUserManagementError: A class usage error
            TableauUserManagementError: An error reported by Tableau Server's REST API
        
        Returns:
            dict -- A dictionary describing the removed user.
        """
        if not self._auth_valid():
            raise TableauUserManagementError('You are not signed in or your token has expired. Please sign in.')
        user_id = self.get_user_id_from_username(username)
        if user_id is None:
            raise TableauUserManagementError('There is no user with a username of {}'.format(username))
        self._api_remove_user(user_id)
        return {
            'removedUser': {
                'id': user_id,
                'name': username
            }
        }
